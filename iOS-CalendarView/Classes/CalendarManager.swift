//
//  CalendarManager.swift
//  Example
//
//  Created by Alberto Chamorro on 29/07/2016.
//  Copyright © 2016 Alberto Chamorro. All rights reserved.
//

import UIKit

public protocol CalendarManagerObserver: class {
    func calendarManagerDidUpdateDate(calendarManager: CalendarManager)
}

class Weak<T: AnyObject> {
    weak var value: T?
    
    init(value: T) {
        self.value = value
    }
}

public class CalendarManager: NSObject {
    public var date: NSDate
    
    let step: Int
    let stepUnit: NSCalendarUnit
    public let calendar: NSCalendar
    public let referenceDate: NSDate
    
    private var observers = [Weak<AnyObject>]()
    
    public init(date: NSDate = NSDate(), step: Int = 1, unit: NSCalendarUnit = .Day, calendar: NSCalendar? = nil) {
        let currentCalendar = calendar ?? NSCalendar.currentCalendar()
        
        self.referenceDate = currentCalendar.previousMonday(fromDate: date)
        self.date = date
        self.step = step
        self.stepUnit = unit
        self.calendar = currentCalendar
    }
    
    public func addDateObserver(observer: CalendarManagerObserver) {
        guard (observers.filter { $0 === observer }.count == 0) else {
            return
        }
        
        observers.append(Weak(value: observer))
    }
    
    public func removeDateObserver(observer: CalendarManagerObserver) {
        var foundIndex: Int?
        
        for i in 0...observers.count {
            if observers[i] === observer {
                foundIndex = i
                
                break
            }
        }
        
        if let foundIndex = foundIndex {
            observers.removeAtIndex(foundIndex)
        }
    }
    
    public func setDate(date newDate: NSDate, byObject object: AnyObject) {
        date = newDate
        
        let observersToNotify = observers.filter { $0 !== object }
        
        for (_, weakObserver) in observersToNotify.enumerate() {
            if let observer = weakObserver.value as? CalendarManagerObserver {
                observer.calendarManagerDidUpdateDate(self)
            }
        }
    }
}

// Navigation
public extension CalendarManager {
    func dateAdvancingFromReferenceDate(by steps: Int) -> NSDate {
        return dateAdvancing(by: steps, fromDate: referenceDate)
    }
    
    func dateAdvancing(by steps: Int, fromDate date: NSDate? = nil) -> NSDate {
        return calendar.dateByAddingUnit(stepUnit,
                                         value: steps*step,
                                         toDate: date ?? self.date,
                                         options: .MatchNextTime)!
    }
    
    func stepsFromReferenceDate(toDate date: NSDate) -> Int {
        var fromDate: NSDate?
        var toDate: NSDate?
        
        calendar.rangeOfUnit(.WeekOfYear, startDate: &fromDate, interval: nil, forDate: referenceDate)
        calendar.rangeOfUnit(.WeekOfYear, startDate:  &toDate, interval: nil, forDate: date)
        
        let difference = calendar.components(.WeekOfYear, fromDate: fromDate!, toDate: toDate!, options: [])
        
        return difference.weekOfYear
    }
}