//
//  CalendarDateManager.swift
//  Example
//
//  Created by Alberto Chamorro on 23/07/2016.
//  Copyright © 2016 Alberto Chamorro. All rights reserved.
//

import Foundation

class CalendarDateManager {
    private let calendar: NSCalendar
    
    init(calendar: NSCalendar = NSCalendar.currentCalendar())
    {
        self.calendar = calendar
    }
}

extension CalendarDateManager {
    func enumerateDays(inMonth month: NSDate, enumerator: (NSDate)->Void) {
        var currentDate = calendar.previousMonday(fromDate: month)
        let totalDays = Int(DaysPerWeek) * Int(WeeksPerMonth)

        for _ in 0..<totalDays {
            enumerator(currentDate)
            currentDate = currentDate.dateByAddingOneDay()
        }
    }
    
    func enumerateDays(inWeek week: NSDate, enumerator: (NSDate)->Void) {
        var currentDate = calendar.previousMonday(fromDate: week)
        let totalDays = Int(DaysPerWeek)
        
        for _ in 0..<totalDays {
            enumerator(currentDate)
            currentDate = currentDate.dateByAddingOneDay()
        }
    }
}

private let NSCalendarMondayWeekDay = 2
extension NSCalendar {
    func previousMonday(fromDate date: NSDate) -> NSDate {
        guard !isDateInMonday(date) else {
            return date
        }
        
        return dateByAddingUnit(.Day, value: -daysToPrevMondayFromDate(date), toDate: date, options: .MatchFirst)!
    }
    
    private func daysToPrevMondayFromDate(date: NSDate) -> Int {
        let weekday = component(.Weekday, fromDate: date)
        
        return (Int(DaysPerWeek) - (NSCalendarMondayWeekDay - weekday)) % Int(DaysPerWeek)
    }
    
}