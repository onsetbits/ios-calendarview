//
//  UIStackView+AddViews.swift
//  Example
//
//  Created by Alberto Chamorro on 31/07/2016.
//  Copyright © 2016 Alberto Chamorro. All rights reserved.
//

import UIKit

@available(iOS 9.0, *)
extension UIStackView {
    func addArrangedSubviews(subviews: [UIView]) {
        for view in subviews {
            self.addArrangedSubview(view)
        }
    }
}

