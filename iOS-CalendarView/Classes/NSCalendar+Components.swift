//
//  NSCalendar+Components.swift
//  Example
//
//  Created by Alberto Chamorro on 22/07/2016.
//  Copyright © 2016 Alberto Chamorro. All rights reserved.
//

import Foundation

public extension NSCalendar {
    func monthSymbolForDate(date: NSDate) -> String {
        let month = component(.Month, fromDate: date)
        
        return monthSymbols[month - 1]
    }
    
    func yearForDate(date: NSDate) -> Int {
        return component(.Year, fromDate: date)
    }
    
    func monthForDate(date: NSDate) -> Int {
        return component(.Month, fromDate: date)
    }
    
    func shortYearForDate(date: NSDate) -> Int {
        let longYear = yearForDate(date)
        let shortYear = longYear - ((longYear / 1000) * 1000)
        
        return shortYear
    }
}