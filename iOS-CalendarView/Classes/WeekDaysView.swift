//
//  WeekDaysView.swift
//  Example
//
//  Created by Alberto Chamorro on 24/07/2016.
//  Copyright © 2016 Alberto Chamorro. All rights reserved.
//

import UIKit

protocol WeekDaysViewDataSource: class {
    func weekDaysView(weekDaysView: WeekDaysView, viewForWeekDay weekday: Weekday) -> UIView
    func prepareWeekDaysView(weekDaysView: WeekDaysView)
}

public class WeekDaysView: UIView {

    private var weekDayViews = [Weekday: UIView]()
    
    weak var dataSource: WeekDaysViewDataSource? {
        didSet {
            initializeUI()
        }
    }
    
    private func initializeUI() {
        let container = createWeekDayViews()
        
        self.addSubview(container)

        container.translatesAutoresizingMaskIntoConstraints = false
        container.widthAnchor.constraintEqualToAnchor(self.widthAnchor).active = true
        container.heightAnchor.constraintEqualToAnchor(self.heightAnchor).active = true
        container.centerXAnchor.constraintEqualToAnchor(self.centerXAnchor).active = true
        container.centerYAnchor.constraintEqualToAnchor(self.centerYAnchor).active = true
        
        dataSource?.prepareWeekDaysView(self)
    }

    private func createWeekDayViews() -> UIView {
        assert(dataSource != nil, "WeekDaysView.dataSource cannot be nil")
        
        if let dataSource = dataSource {
            let container = UIStackView()
            container.distribution = .FillEqually
            
            for weekday in Weekday.allValues {
                let view = dataSource.weekDaysView(self, viewForWeekDay: weekday)
                weekDayViews[weekday] = view
                
                container.addArrangedSubview(view)
            }
            
            return container
        }
        
        fatalError("WeekDaysView.dataSource cannot be nil")
    }
}

extension WeekDaysView {
    func viewForWeekday(weekday: Weekday) -> UIView {
        return weekDayViews[weekday]!
    }
}