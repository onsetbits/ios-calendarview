//
//  NSCalendar+DateIn.swift
//  Example
//
//  Created by Alberto Chamorro on 22/07/2016.
//  Copyright © 2016 Alberto Chamorro. All rights reserved.
//

import UIKit

public extension NSCalendar {
    func isDateInMonth(date: NSDate, month: Int) -> Bool {
        return component(.Month, fromDate: date) == month
    }
        
    func isDateInMonday(date: NSDate) -> Bool {
        return component(.Weekday, fromDate: date) == 2
    }
}
