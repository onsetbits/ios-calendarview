//
//  DayView.swift
//  Example
//
//  Created by Alberto Chamorro on 22/07/2016.
//  Copyright © 2016 Alberto Chamorro. All rights reserved.
//

import UIKit

public class DayView: UIView {
    public var label: UILabel!

    private var dayNumber: Int {
        get {
            return NSCalendar.currentCalendar().component(.Day, fromDate: date!)
        }
    }
    
    public var date: NSDate? {
        didSet {
            label.text = "\(dayNumber)"
        }
    }
    
    lazy var circleView: UIView? = {[unowned self] in
        return self.createCircleBackgroundView()
    }()
    
    lazy var eventDotView: UIView? = {[unowned self] in
        return self.createEventDotView()
    }()
    
    convenience init() {
        self.init(frame: CGRectZero)
    }
    
    override init(frame: CGRect) {
        label = UILabel(frame: frame)
        label.textAlignment = .Center
        
        super.init(frame: frame)
        
        self.addSubview(label)
        
        label.translatesAutoresizingMaskIntoConstraints = false
        label.widthAnchor.constraintEqualToAnchor(self.widthAnchor).active = true
        label.heightAnchor.constraintEqualToAnchor(self.heightAnchor, multiplier: 1/2).active = true
        label.centerXAnchor.constraintEqualToAnchor(self.centerXAnchor).active = true
        label.topAnchor.constraintEqualToAnchor(self.topAnchor, constant: 5).active = true
    }
    
    required public init?(coder aDecoder: NSCoder) {
        fatalError("init(coder aDecoder:) not implemented")
    }

}

extension DayView {
    private func createEventDotView() -> UIView {
        let dotView = UIView()
        
        dotView.backgroundColor = UIColor.whiteColor()
        
        self.addSubview(dotView)
        
        let dotDiameter: CGFloat = 3
        dotView.translatesAutoresizingMaskIntoConstraints = false
        dotView.centerXAnchor.constraintEqualToAnchor(label.centerXAnchor).active = true
        dotView.bottomAnchor.constraintEqualToAnchor(self.bottomAnchor, constant: -5).active = true
        dotView.heightAnchor.constraintEqualToConstant(dotDiameter).active = true
        dotView.widthAnchor.constraintEqualToConstant(dotDiameter).active = true
        dotView.layoutIfNeeded()
        
        dotView.layer.cornerRadius = dotDiameter*0.5
        
        return dotView
    }
    
    private func createCircleBackgroundView() -> UIView {
        let view = UIView()
        
        self.addSubview(view)
        self.sendSubviewToBack(view)
        
        view.translatesAutoresizingMaskIntoConstraints = false
        view.centerYAnchor.constraintEqualToAnchor(label.centerYAnchor).active = true
        view.centerXAnchor.constraintEqualToAnchor(label.centerXAnchor).active = true
        view.widthAnchor.constraintEqualToAnchor(label.heightAnchor, constant: 5).active = true
        view.heightAnchor.constraintEqualToAnchor(label.heightAnchor, constant: 5).active = true
        view.layoutIfNeeded()
        
        view.layer.cornerRadius = view.frame.height / 2
        view.backgroundColor = UIColor.blueColor()
        
        return view
    }
}

public extension DayView {
    public func showCircledBackground(configuration: ((UIView) -> Void)? = nil) {
        circleView?.hidden = false
        configuration?(circleView!)
    }
    
    public func showEventDot(configuration: ((UIView) -> Void)? = nil) {
        eventDotView?.hidden = false
        configuration?(eventDotView!)
    }
    
    func resetStyle() {
        circleView?.hidden = true
        eventDotView?.hidden = true
        self.label.textColor = UIColor.blackColor()
        self.label.font = self.label.font.normal()
    }
}
