//
//  CalendarViewDataSource.swift
//  Example
//
//  Created by Alberto Chamorro on 31/07/2016.
//  Copyright © 2016 Alberto Chamorro. All rights reserved.
//

import UIKit

public protocol CalendarViewDataSource: class {
    func calendarView(calendarView: CalendarView, viewForDate day: NSDate) -> UIView?
    func calendarView(calendarView: CalendarView, prepareDayView dayView: UIView)
    func calendarView(calendarView: CalendarView, viewForWeekDay weekday: Weekday) -> UIView
    func calendarView(calendarView: CalendarView, prepareWeekDaysView view: UIView)
}

public extension CalendarViewDataSource {
    func calendarView(calendarView: CalendarView, viewForDate date: NSDate) -> UIView? {
        let dayView = DayView()
        dayView.date = date
        
        return dayView
    }

    func calendarView(calendarView: CalendarView, viewForWeekDay weekday: Weekday) -> UIView {
        let label = UILabel()
        label.text = weekday.localizedShortString.uppercaseString
        label.textAlignment = .Center
        label.textColor = UIColor.blackColor()
        
        return label
    }
    
    func calendarView(calendarView: CalendarView, prepareWeekDaysView view: UIView) {
        // Do nothing by default
    }
    
    func calendarView(calendarView: CalendarView, prepareDayView dayView: UIView) {
        // Do nothing by default
    }
}
