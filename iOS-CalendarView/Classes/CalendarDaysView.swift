//
//  CalendarDaysView.swift
//  Example
//
//  Created by Alberto Chamorro on 15/07/2016.
//  Copyright © 2016 Alberto Chamorro. All rights reserved.
//

import UIKit

protocol CalendarDaysViewDelegate: class {
    func calendarDaysView(calendarDaysView: CalendarDaysView, viewForDate date: NSDate) -> UIView?
    func calendarDaysView(calendarDaysView: CalendarDaysView, didSelectDay dayView: UIView)
    func calendarDaysView(calendarDaysView: CalendarDaysView, prepareDayView dayView: UIView)
}

class CalendarDaysView: UIView {
    weak var delegate: CalendarDaysViewDelegate?
    var calendarDaysCollectionView: UICollectionView!
    var calendarDaysDataSource: WeeksUICollectionViewDataSource!
    
    var calendarManager: CalendarManager! {
        didSet {
            calendarManager.addDateObserver(self)
            calendarDaysDataSource.calendarManager = calendarManager
            calendarDaysCollectionView.reloadData()
        }
    }
 
    override init(frame: CGRect) {
        super.init(frame: frame)
        
        calendarDaysCollectionView = createContainer()

        self.addSubview(calendarDaysCollectionView)
        calendarDaysCollectionView.topAnchor.constraintEqualToAnchor(self.topAnchor).active = true
        calendarDaysCollectionView.leftAnchor.constraintEqualToAnchor(self.leftAnchor).active = true
        calendarDaysCollectionView.widthAnchor.constraintEqualToAnchor(self.widthAnchor).active = true
        calendarDaysCollectionView.heightAnchor.constraintEqualToAnchor(self.heightAnchor).active = true
    }
    
    func createContainer() -> UICollectionView {
        let layout = UICollectionViewFlowLayout()
        layout.scrollDirection = .Horizontal
        
        let calendarDaysView = UICollectionView(frame: CGRectZero, collectionViewLayout: layout)
        calendarDaysView.translatesAutoresizingMaskIntoConstraints = false
        calendarDaysView.delegate = self
        
        calendarDaysDataSource = WeeksUICollectionViewDataSource(collectionView: calendarDaysView)
        calendarDaysDataSource.cellDelegate = self
        calendarDaysView.dataSource = calendarDaysDataSource
        
        calendarDaysView.pagingEnabled = true
        calendarDaysView.showsVerticalScrollIndicator = false
        calendarDaysView.showsHorizontalScrollIndicator = false
        calendarDaysView.bounces = false

        return calendarDaysView
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
}

extension CalendarDaysView: CalendarUICollectionViewCellDelegate {
    func calendarViewCell(calendarViewCell: CalendarUICollectionViewCell, viewForDate date: NSDate) -> UIView? {
        return delegate?.calendarDaysView(self, viewForDate: date)
    }
    func calendarViewCell(calendarViewCell: CalendarUICollectionViewCell, didSelectDay dayView: UIView) {
        delegate?.calendarDaysView(self, didSelectDay: dayView)
    }

    func calendarViewCell(calendarViewCell: CalendarUICollectionViewCell, prepareDayView dayView: UIView) {
        delegate?.calendarDaysView(self, prepareDayView: dayView)
    }
}

extension CalendarDaysView: CalendarManagerObserver {
    func calendarManagerDidUpdateDate(calendarManager: CalendarManager) {
        let item = calendarManager.stepsFromReferenceDate(toDate: calendarManager.date)
        let newOffset = CGPoint(x: CGFloat(item)*calendarDaysCollectionView.frame.width, y: 0)
        let shouldAnimateScroll = abs(item - calendarDaysCollectionView.indexPathsForVisibleItems().last!.item) < 2

        calendarDaysCollectionView.setContentOffset(newOffset, animated: shouldAnimateScroll)
    }
}

// MARK: UICollectionViewDelegateFlowLayout
extension CalendarDaysView: UICollectionViewDelegateFlowLayout {
    
    func collectionView(collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAtIndexPath indexPath: NSIndexPath) -> CGSize {
        return collectionView.frame.size
    }
    
    func collectionView(collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, insetForSectionAtIndex section: Int) -> UIEdgeInsets {
        return UIEdgeInsetsZero
    }
    
    func collectionView(collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumInteritemSpacingForSectionAtIndex section: Int) -> CGFloat {
        return 0
    }
    
    func collectionView(collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumLineSpacingForSectionAtIndex section: Int) -> CGFloat {
        return 0
    }
}