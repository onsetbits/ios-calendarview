//
//  MonthViewDelegate.swift
//  Example
//
//  Created by Alberto Chamorro on 22/07/2016.
//  Copyright © 2016 Alberto Chamorro. All rights reserved.
//

import UIKit

internal protocol CalendarUICollectionViewCellDelegate: class {
    func calendarViewCell(calendarViewCell: CalendarUICollectionViewCell, prepareDayView dayView: UIView)
    func calendarViewCell(calendarViewCell: CalendarUICollectionViewCell, viewForDate date: NSDate) -> UIView?
    func calendarViewCell(calendarViewCell: CalendarUICollectionViewCell, didSelectDay dayView: UIView)
}