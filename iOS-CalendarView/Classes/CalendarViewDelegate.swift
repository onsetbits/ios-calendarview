//
//  CalendarViewDelegate.swift
//  Example
//
//  Created by Alberto Chamorro on 31/07/2016.
//  Copyright © 2016 Alberto Chamorro. All rights reserved.
//

import UIKit

public protocol CalendarViewDelegate: class {
    func calendarView(calendarView: CalendarView, didSelectDay dayView: UIView)
}

extension CalendarDaysViewDelegate {
    func calendarView(calendarView: CalendarView, didSelectDay dayView: UIView) { /* do nothing */ }
}