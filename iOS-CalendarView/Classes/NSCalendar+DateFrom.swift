//
//  NSCalendar+DateFrom.swift
//  Example
//
//  Created by Alberto Chamorro on 22/07/2016.
//  Copyright © 2016 Alberto Chamorro. All rights reserved.
//

import Foundation

public extension NSCalendar {
    func dateFromMonth(month: Int, year: Int? = nil) -> NSDate? {
        let year = year ?? component(.Year, fromDate: NSDate())
        let components = NSDateComponents()
        components.day = 1
        components.month = month
        components.year = year
        components.hour = 12
        
        return dateFromComponents(components)
    }
    
    func dateFromWeekOfYear(weekOfYear: Int, year: Int? = nil) -> NSDate? {
        let year = year ?? component(.Year, fromDate: NSDate())
        
        let components = NSDateComponents()
        components.year = year
        components.weekOfYear = weekOfYear
        components.weekday = 2
        components.hour = 12
        
        return dateFromComponents(components)
    }
    
    func dateByAdding(months months: Int, toDate date: NSDate) -> NSDate? {
        return dateByAddingUnit(.Month, value: months, toDate: date, options: .MatchNextTime)
    }
}