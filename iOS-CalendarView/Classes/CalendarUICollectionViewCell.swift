//
//  MonthView.swift
//  Example
//
//  Created by Alberto Chamorro on 17/07/2016.
//  Copyright © 2016 Alberto Chamorro. All rights reserved.
//

import UIKit

let DaysPerWeek: CGFloat = 7
let WeeksPerMonth: CGFloat = 6

internal class CalendarUICollectionViewCell: UICollectionViewCell {
    private var cellSize: CGSize!
    
    weak var delegate: CalendarUICollectionViewCellDelegate?

    private lazy var days: [UIView]! = { [unowned self] in
        return self.createAndAddDays(numberOfDays: Int(DaysPerWeek*WeeksPerMonth))
    }()
    
    var date: NSDate? {
        didSet {
            reloadData()
        }
    }
    
    override convenience init(frame: CGRect) {
        self.init(frame: frame, numberOfDays: Int(DaysPerWeek*WeeksPerMonth))
    }
    
    init(frame: CGRect, numberOfDays: Int) {
        cellSize = CGSize(width: frame.width/DaysPerWeek,
                          height: frame.height/(CGFloat(numberOfDays)/DaysPerWeek))
        
        super.init(frame: frame)
    }
    
    func createAndAddDays(numberOfDays numberOfDays: Int) -> [UIView]? {
        assert(delegate != nil, "createAndAddDays(origin:days:), the delegate has not been set")
        guard let delegate = delegate else {
            NSLog("createAndAddDays(origin:days:), the delegate has not been set")
            
            return nil
        }
        
        var days = [UIView]()
        days.reserveCapacity(numberOfDays)
        var currentDayFrame: CGRect
        
        for i in 0..<numberOfDays {
            currentDayFrame = CGRect(origin: originFor(day: i, originY: 0), size: cellSize)

            if let currentDay = delegate.calendarViewCell(self, viewForDate: NSDate()) {
                currentDay.frame = currentDayFrame
                currentDay.userInteractionEnabled = true
                currentDay.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(didSelectDay)))
                
                days.append(currentDay)
                self.addSubview(currentDay)
            }
        }
        
        return days
    }
    
    func reloadData() {
        assert(delegate != nil, "createAndAddDays(origin:days:), the delegate has not been set")
        guard let delegate = delegate else {
            NSLog("createAndAddDays(origin:days:), the delegate has not been set")
            
            return
        }
        
        let dateManager = CalendarDateManager()
        var i = 0
        var dayView: DayView!
        dateManager.enumerateDays(inWeek: date!) { (day) in
            dayView = self.days[i] as! DayView
            dayView.date = day
            dayView.resetStyle()
            
            delegate.calendarViewCell(self, prepareDayView: dayView)
            
            i += 1
        }
    }
    
    func originFor(day day: Int, originY: CGFloat) -> CGPoint {
        let relX = cellSize.width * CGFloat(CGFloat(day) % DaysPerWeek)
        let relY = cellSize.height * CGFloat(Int(CGFloat(day) / DaysPerWeek))
        
        return CGPoint(x: relX, y: originY + relY)
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
    }
}

extension CalendarUICollectionViewCell {
    func didSelectDay(sender: UITapGestureRecognizer) {
        if let day = sender.view as? DayView {
            delegate?.calendarViewCell(self, didSelectDay: day)
        } else {
            NSLog("didSelectDay(sender:) not using DayView instances")
        }
    }
}
