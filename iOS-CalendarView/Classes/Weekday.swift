//
//  Weekday.swift
//  Example
//
//  Created by Alberto Chamorro on 27/07/2016.
//  Copyright © 2016 Alberto Chamorro. All rights reserved.
//

import Foundation

public enum Weekday: Int {
    case Sunday = 0, Monday, Tuesday, Wednesday, Thursday, Friday, Saturday
    
    static let allValues = [Monday, Tuesday, Wednesday, Thursday, Friday, Saturday, Sunday]
    
    public var localizedLongString: String {
        get {
            let weekdaySymbols = NSCalendar.currentCalendar().weekdaySymbols
            
            return weekdaySymbols[self.rawValue]
        }
    }
    
    public var localizedShortString: String {
        get {
            let weekdaySymbols = NSCalendar.currentCalendar().shortWeekdaySymbols
            
            return weekdaySymbols[self.rawValue]
        }
    }
    
    func isInDate(date: NSDate) -> Bool {
        return (self.rawValue + 1) == NSCalendar.currentCalendar().component(.Weekday, fromDate: date)
    }
}
