//
//  CalendarView.swift
//  Example
//
//  Created by Alberto Chamorro on 03/07/2016.
//  Copyright © 2016 Alberto Chamorro. All rights reserved.
//

import UIKit

public class CalendarView: UIView {
    
    private var calendarDaysView: CalendarDaysView!
    private var weekDaysHeaderView: WeekDaysView!
    
    public var calendarManager: CalendarManager {
        get {
            return calendarDaysView.calendarManager
        }
        
        set {
            calendarDaysView.calendarManager = newValue
        }
    }
    
    public weak var dataSource: CalendarViewDataSource? {
        didSet {
            initializeUI()
            
            weekDaysHeaderView.dataSource = self
        }
    }
    
    public weak var delegate: CalendarViewDelegate?
    
    private func initializeUI() {
        let container = createContainer(andAddTo: self)
        container.addArrangedSubviews([createWeekdaysHeaderView(), createCalendarContentView()])
        
        addLayoutConstraints(ofContainerView: container, to: self)
    }
    
    private func createContainer(andAddTo parent: UIView) -> UIStackView {
        let container = UIStackView()
        container.axis = .Vertical
        container.distribution = .Fill
        
        parent.addSubview(container)
        
        return container
    }
    
    private func createCalendarContentView() -> UIView {
        let view = CalendarDaysView(frame: CGRectZero)
        
        calendarDaysView = view
        calendarDaysView.delegate = self
        
        return view
    }

    private func createWeekdaysHeaderView() -> UIView {
        let weekdayHeader = WeekDaysView(frame: CGRectZero)
        
        weekDaysHeaderView = weekdayHeader
        
        return weekdayHeader
    }
}

extension CalendarView: WeekDaysViewDataSource {
    func weekDaysView(weekDaysView: WeekDaysView, viewForWeekDay weekday: Weekday) -> UIView {
        assert(dataSource != nil, "CalendarView must have a delegate to initialize the UI")
        
        return (dataSource?.calendarView(self, viewForWeekDay: weekday))!
    }
    
    func prepareWeekDaysView(weekDaysView: WeekDaysView) {
        assert(dataSource != nil, "CalendarView must have a delegate to initialize the UI")

        dataSource?.calendarView(self, prepareWeekDaysView: weekDaysView)
    }
}

extension CalendarView: CalendarDaysViewDelegate {
    func calendarDaysView(calendarDaysView: CalendarDaysView, viewForDate date: NSDate) -> UIView? {
        return dataSource?.calendarView(self, viewForDate: date)
    }
    
    func calendarDaysView(calendarDaysView: CalendarDaysView, prepareDayView dayView: UIView) {
        dataSource?.calendarView(self, prepareDayView: dayView)
    }
    
    func calendarDaysView(calendarDaysView: CalendarDaysView, didSelectDay dayView: UIView) {
        delegate?.calendarView(self, didSelectDay: dayView)
    }
}

private extension CalendarView {
    private func addLayoutConstraints(ofContainerView view: UIView, to parent: UIView) {
        view.translatesAutoresizingMaskIntoConstraints = false
        view.centerXAnchor.constraintEqualToAnchor(parent.centerXAnchor).active = true
        view.topAnchor.constraintEqualToAnchor(parent.topAnchor).active = true
        view.heightAnchor.constraintEqualToAnchor(parent.heightAnchor).active = true
        view.widthAnchor.constraintEqualToAnchor(parent.widthAnchor).active = true
    }
}