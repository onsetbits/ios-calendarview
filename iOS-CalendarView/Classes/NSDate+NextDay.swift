//
//  NSDate+NextDay.swift
//  Example
//
//  Created by Alberto Chamorro on 22/07/2016.
//  Copyright © 2016 Alberto Chamorro. All rights reserved.
//

import Foundation

extension NSDate {
    func dateByAddingOneDay() -> NSDate {
        // 24 hours * 60 mins/hour * 60 secs/min
        return dateByAddingTimeInterval(24*60*60)
    }
}