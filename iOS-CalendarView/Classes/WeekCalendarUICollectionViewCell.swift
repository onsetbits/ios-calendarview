//
//  WeekCalendarUICollectionViewCell.swift
//  Example
//
//  Created by Alberto Chamorro on 23/07/2016.
//  Copyright © 2016 Alberto Chamorro. All rights reserved.
//

import UIKit

class WeekCalendarUICollectionViewCell: CalendarUICollectionViewCell {
    init(frame: CGRect) {
        super.init(frame: frame, numberOfDays: 7)
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
    }
}