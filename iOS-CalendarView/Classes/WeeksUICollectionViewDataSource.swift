//
//  WeeksUICollectionViewDataSource.swift
//  Example
//
//  Created by Alberto Chamorro on 23/07/2016.
//  Copyright © 2016 Alberto Chamorro. All rights reserved.
//

import UIKit

class WeeksUICollectionViewDataSource: NSObject, UICollectionViewDataSource {
    var numberOfWeeksToShow = 53 // Number of Weeks Per Year
    
    var cellDelegate: CalendarUICollectionViewCellDelegate?
    var calendarManager: CalendarManager? {
        didSet {
            collectionView?.reloadData()
        }
    }
    
    weak var collectionView: UICollectionView?

    init(collectionView: UICollectionView) {
        self.collectionView = collectionView
        
        collectionView.registerClass(WeekCalendarUICollectionViewCell.self,
                                     forCellWithReuseIdentifier: "WeeksCollectionViewReuseIdentifier")
    }
    
    func numberOfSectionsInCollectionView(collectionView: UICollectionView) -> Int {
        return calendarManager != nil ? numberOfWeeksToShow : 0
    }
    
    func collectionView(collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return numberOfWeeksToShow
    }
    
    func collectionView(collectionView: UICollectionView, cellForItemAtIndexPath indexPath: NSIndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCellWithReuseIdentifier("WeeksCollectionViewReuseIdentifier", forIndexPath: indexPath) as! CalendarUICollectionViewCell
        
        cell.delegate = cellDelegate
        cell.date = calendarManager?.dateAdvancingFromReferenceDate(by: indexPath.item)
        
        return cell
    }
}