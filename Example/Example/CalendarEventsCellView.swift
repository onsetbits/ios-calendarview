//
//  CalendarEventsCellView.swift
//  Example
//
//  Created by Alberto Chamorro on 29/07/2016.
//  Copyright © 2016 Alberto Chamorro. All rights reserved.
//

import UIKit

class CalendarEventsCellView: UICollectionViewCell {
    weak var weekEventsView: EventsView!
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
    }
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        
        weekEventsView = EventsView()
        weekEventsView.backgroundColor = UIColor.whiteColor()
        self.addSubview(weekEventsView)
        
        weekEventsView.translatesAutoresizingMaskIntoConstraints = false
        weekEventsView.widthAnchor.constraintEqualToAnchor(self.widthAnchor).active = true
        weekEventsView.heightAnchor.constraintEqualToAnchor(self.heightAnchor).active = true
        weekEventsView.centerXAnchor.constraintEqualToAnchor(self.centerXAnchor).active = true
        weekEventsView.centerYAnchor.constraintEqualToAnchor(self.centerYAnchor).active = true
    }
}
