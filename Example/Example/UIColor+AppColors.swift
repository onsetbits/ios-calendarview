//
//  UIColor+AppColors.swift
//  Example
//
//  Created by Alberto Chamorro on 25/07/2016.
//  Copyright © 2016 Alberto Chamorro. All rights reserved.
//

import UIKit

extension UIColor {
    class func withRGB(r red: Int, g green: Int, b blue: Int) -> UIColor {
        let factor: CGFloat = 1/255
        
        return UIColor(red: CGFloat(red)*factor, green: CGFloat(green)*factor, blue: CGFloat(blue)*factor, alpha: 1.0)
    }
    
    class func textNormalColor() -> UIColor {
        return withRGB(r: 100, g: 100, b: 100)
    }
    
    class func backgroundColor() -> UIColor {
        return withRGB(r: 33, g: 33, b: 33)
    }
    
    class func textHighlightColor() -> UIColor {
        return withRGB(r: 228, g: 228, b: 228)
    }
}

extension UIColor {
    class var randomColor: UIColor {
        return UIColor(red: CGFloat(arc4random_uniform(256))/255,
                       green: CGFloat(arc4random_uniform(256))/255,
                       blue: CGFloat(arc4random_uniform(256))/255,
                       alpha: 1)
    }
}
