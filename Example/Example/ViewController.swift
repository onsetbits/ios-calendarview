//
//  ViewController.swift
//  Example
//
//  Created by Alberto Chamorro on 03/07/2016.
//  Copyright © 2016 Alberto Chamorro. All rights reserved.
//

import UIKit
import iOS_CalendarView

class ViewController: UIViewController {

    @IBOutlet private weak var calendarView: CalendarView!
    @IBOutlet private weak var calendarHeader: UILabel!
    @IBOutlet private weak var calendarEventsView: CalendarEventsView!
    @IBOutlet weak var calendarViewHeight: NSLayoutConstraint!
    @IBOutlet weak var calendarViewWeekModeHeight: NSLayoutConstraint!
    
    private var eventsRepository: EventsRepository!
    private var calendarManager: CalendarManager!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // Do any additional setup after loading the view, typically from a nib.
        eventsRepository = EventsRepository()
        populateEvents()
        
        calendarManager = CalendarManager(unit: .WeekOfYear)
        calendarEventsView.calendarManager = calendarManager
        calendarEventsView.eventsRepository = eventsRepository
        calendarManager.addDateObserver(self)
        
        calendarView.dataSource = self
        calendarView.delegate = self
        calendarView.calendarManager = calendarManager
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    override func viewDidAppear(animated: Bool) {
        calendarManagerDidUpdateDate(calendarManager)
    }
}

extension ViewController: CalendarManagerObserver {
    func calendarManagerDidUpdateDate(calendarManager: CalendarManager) {
        let month = calendarManager.calendar.component(.Month, fromDate: calendarManager.date)
        let monthSymbol = calendarManager.calendar.monthSymbols[month - 1]
        let year = calendarManager.calendar.shortYearForDate(calendarManager.date)
        
        self.calendarHeader.text = "\(monthSymbol) '\(year)"
    }
}

extension ViewController: CalendarViewDataSource {
    func calendarView(calendarview: CalendarView, prepareWeekDaysView weekDaysView: WeekDaysView) {
        weekDaysView.backgroundColor = UIColor.backgroundColor()
    }
            
    func calendarView(calendarView: CalendarView, prepareDayView dayView: UIView) {
        let calendar = NSCalendar.currentCalendar()
        
        if let view = dayView as? DayView, let viewDate = view.date {
            view.backgroundColor = UIColor.backgroundColor()
            
            if calendar.isDateInWeekend(viewDate) {
                view.label.textColor = UIColor.brownColor()
            } else if !calendar.isDateInMonth(viewDate, month: calendar.component(.Month, fromDate: view.date!)) {
                view.label.textColor = UIColor.textNormalColor()
            } else {
                view.label.textColor = UIColor.textHighlightColor()
            }
            
            if calendar.isDateInToday(viewDate) {
                view.label.font = view.label.font.bold()
                view.label.textColor = UIColor.whiteColor()
                view.showCircledBackground() { circle in
                    circle.backgroundColor = UIColor.whiteColor().colorWithAlphaComponent(0.2)
                    circle.transform = CGAffineTransformScale(CGAffineTransformIdentity, 1.1, 1.1)
                }
            }
            
            if eventsRepository.eventsForDate(viewDate).count > 0 {
                view.showEventDot()
            }
        }
    }
    
    func calendarView(calendarView: CalendarView, viewForWeekDay weekday: Weekday) -> UIView {
        let label = UILabel()
        label.textColor = UIColor.textNormalColor()
        label.textAlignment = .Center
        label.backgroundColor = UIColor.backgroundColor()
        label.opaque = false
        label.text = weekday.localizedShortString.uppercaseString
        
        return label
    }
}

extension ViewController: CalendarViewDelegate {
    func calendarView(calendarView: CalendarView, didSelectDay dayView: UIView) { }
}

extension ViewController {
    func populateEvents() {
        let locations = ["Tarantos", "Palacio del Flamenco", "Tablao de Carmen", "Cordobes", "Teatro Poliorama", "Teatro Apolo", "Palau de la Música Catalana"]
        
        for _ in 0...80 {
            let dateComponents = NSDateComponents()
            dateComponents.day = Int(arc4random_uniform(28))
            dateComponents.month = NSCalendar.currentCalendar().component(.Month, fromDate: NSDate()) + Int(arc4random_uniform(6))
            dateComponents.year = NSCalendar.currentCalendar().component(.Year, fromDate: NSDate())
            dateComponents.hour = Int(arc4random_uniform(23))
            dateComponents.minute = Int(arc4random_uniform(4)) * 15
            
            let eventDate = NSCalendar.currentCalendar().dateFromComponents(dateComponents)
            let location = locations[Int(arc4random_uniform(UInt32(locations.count)))]
            let event = CalendarEvent(date: eventDate!,
                                      location: location)
            
            eventsRepository.addEvent(event)
        }
    }
}


