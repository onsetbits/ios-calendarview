//
//  WeekEventsView.swift
//  Example
//
//  Created by Alberto Chamorro on 27/07/2016.
//  Copyright © 2016 Alberto Chamorro. All rights reserved.
//

import UIKit

class EventsView: UICollectionView {
    
    var visibleEvents: [CalendarEvent]? {
        didSet {
            reloadData()
        }
    }
    
    lazy var dateFormatter: NSDateFormatter = {
       let formatter = NSDateFormatter()
        formatter.dateFormat = "HH:mm"
        
        return formatter
    }()
    
    init() {
        let layout = UICollectionViewFlowLayout()
        layout.scrollDirection = .Vertical

        super.init(frame: CGRectZero, collectionViewLayout: layout)
        
        initUI()
    }
    
    func initUI() {
        let nib = UINib(nibName: "DayEventsViewCell", bundle: NSBundle(forClass: DayEventsViewCell.self))
        self.registerNib(nib, forCellWithReuseIdentifier: "DayEventsViewCell")
        self.dataSource = self
        self.delegate = self
        
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        
        initUI()
        
        let layout = UICollectionViewFlowLayout()
        layout.scrollDirection = .Vertical
        self.collectionViewLayout = layout
    }
}

extension EventsView: UICollectionViewDataSource, UICollectionViewDelegateFlowLayout {
    func collectionView(collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAtIndexPath indexPath: NSIndexPath) -> CGSize {
        return CGSize(width: self.frame.width, height: 80)
    }
    
    override func numberOfSections() -> Int {
        return 1
    }
    
    func collectionView(collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return visibleEvents?.count ?? 0
    }
    
    func collectionView(collectionView: UICollectionView, cellForItemAtIndexPath indexPath: NSIndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCellWithReuseIdentifier("DayEventsViewCell", forIndexPath: indexPath) as! DayEventsViewCell
        
        if let visibleEvents = visibleEvents {
            let item = visibleEvents[indexPath.item]
            
            let calendar = NSCalendar.currentCalendar()
            let weekday = calendar.component(.Weekday, fromDate: item.date)
            let weekdayString = NSCalendar.currentCalendar().shortWeekdaySymbols[weekday - 1]
            cell.weekDayLabel.text = weekdayString
            
            cell.dayNumberLabel.text = "\(calendar.component(.Day, fromDate: item.date))"
            cell.timeLabel.text = "\(dateFormatter.stringFromDate(item.date))"
            cell.titleLabel.text = item.location
        }
        
        return cell
    }
}