//
//  EventsRepository.swift
//  Example
//
//  Created by Alberto Chamorro on 27/07/2016.
//  Copyright © 2016 Alberto Chamorro. All rights reserved.
//

import Foundation

struct EventsRepository {
    private var allEvents = [String:[CalendarEvent]]()
    
    mutating func addEvent(event: CalendarEvent) {
        let stringID = stringIDFromEvent(event)
        
        if var dayEvents = allEvents[stringID] {
            dayEvents.append(event)
        } else {
            allEvents[stringID] = [event]
        }
    }
    
    func eventsForDate(date: NSDate) -> [CalendarEvent] {
        let stringID = stringIDFromDate(date)
        
        if let dayEvents = allEvents[stringID] {
            return dayEvents
        }
        
        return []
    }
    
    func eventsForWeekOfYear(date: NSDate) -> [CalendarEvent] {
        let weekOfYear = NSCalendar.currentCalendar().component(.WeekOfYear, fromDate: date)
        let startDate = NSCalendar.currentCalendar().dateFromWeekOfYear(weekOfYear)!

        return (0...6).map{
            NSCalendar.currentCalendar().dateByAddingUnit(.Day, value: $0, toDate: startDate, options: .MatchNextTime)!
        }.map{
            allEvents[stringIDFromDate($0)]
        }.reduce([], combine: {$0 + ($1 ?? [])})
    }
}

extension EventsRepository {
    func stringIDFromDate(date: NSDate) -> String {
        let calendar = NSCalendar.currentCalendar()
        let stringID = "\(calendar.component(.Year, fromDate: date))\(calendar.component(.Month, fromDate: date))\(calendar.component(.Day, fromDate: date))"
        
        return stringID

    }
    func stringIDFromEvent(event: CalendarEvent) -> String {
        return stringIDFromDate(event.date)
    }
}