//
//  Event.swift
//  Example
//
//  Created by Alberto Chamorro on 27/07/2016.
//  Copyright © 2016 Alberto Chamorro. All rights reserved.
//

import Foundation

struct CalendarEvent {
    let date: NSDate
    let location: String
}