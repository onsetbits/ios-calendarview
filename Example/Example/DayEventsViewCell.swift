//
//  DayEventsViewCell.swift
//  Example
//
//  Created by Alberto Chamorro on 27/07/2016.
//  Copyright © 2016 Alberto Chamorro. All rights reserved.
//

import UIKit

class DayEventsViewCell: UICollectionViewCell {
    @IBOutlet weak var weekDayLabel: UILabel!
    @IBOutlet weak var dayNumberLabel: UILabel!
    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var timeLabel: UILabel!
}