//
//  CalendarEventsView.swift
//  Example
//
//  Created by Alberto Chamorro on 29/07/2016.
//  Copyright © 2016 Alberto Chamorro. All rights reserved.
//

import UIKit
import iOS_CalendarView

class CalendarEventsView: UICollectionView {
    var calendarManager: CalendarManager? {
        didSet {
            calendarManager?.addDateObserver(self)
            reloadData()
        }
    }
    
    var eventsRepository: EventsRepository? {
        didSet {
            reloadData()
        }
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)

        self.pagingEnabled = true
        self.showsHorizontalScrollIndicator = false
        self.showsVerticalScrollIndicator = false
        self.bounces = false
        
        self.delegate = self
        self.dataSource = self
        
        let layout = UICollectionViewFlowLayout()
        layout.scrollDirection = .Horizontal
        self.collectionViewLayout = layout
        
        self.registerClass(CalendarEventsCellView.self, forCellWithReuseIdentifier: "CalendarEventsCellView")
        self.backgroundColor = UIColor.randomColor
    }
}

extension CalendarEventsView: CalendarManagerObserver {
    func calendarManagerDidUpdateDate(calendarManager: CalendarManager) {
        let indexPath = CGFloat(calendarManager.stepsFromReferenceDate(toDate: calendarManager.date))
        
        self.setContentOffset(CGPoint(x: (indexPath * self.frame.width), y: 0), animated: true)
    }
}

extension CalendarEventsView: UICollectionViewDataSource {
    func numberOfSectionsInCollectionView(collectionView: UICollectionView) -> Int {
        return (calendarManager != nil && eventsRepository != nil) ? 1 : 0
    }
    
    func collectionView(collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return 52
    }
    
    func collectionView(collectionView: UICollectionView, cellForItemAtIndexPath indexPath: NSIndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCellWithReuseIdentifier("CalendarEventsCellView", forIndexPath: indexPath) as! CalendarEventsCellView

        let date = calendarManager?.dateAdvancing(by: indexPath.item ,fromDate: calendarManager!.referenceDate)
        cell.weekEventsView.visibleEvents = eventsRepository?.eventsForWeekOfYear(date!)
        
        return cell
    }
}

extension CalendarEventsView: UICollectionViewDelegate {
    func scrollViewDidEndDecelerating(scrollView: UIScrollView) {
        let indexPath = Int(scrollView.contentOffset.x / self.frame.width)
        
        calendarManager?.setDate(date: calendarManager!.dateAdvancingFromReferenceDate(by: indexPath), byObject: self)
    }
}

extension CalendarEventsView: UICollectionViewDelegateFlowLayout {
    func collectionView(collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAtIndexPath indexPath: NSIndexPath) -> CGSize {
        return self.frame.size
    }
    
    func collectionView(collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, insetForSectionAtIndex section: Int) -> UIEdgeInsets {
        return UIEdgeInsetsZero
    }
    
    func collectionView(collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumInteritemSpacingForSectionAtIndex section: Int) -> CGFloat {
        return 0
    }
    
    func collectionView(collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumLineSpacingForSectionAtIndex section: Int) -> CGFloat {
        return 0
    }

}
